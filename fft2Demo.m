% simple demo for 2D fourier transform good practice 
% inspired from cell #4 in https://github.com/Waller-Lab/DiffuserCam-Tutorial/blob/master/tutorial/GD.ipynb

img=zeros(255);
img(round(.5*size(img,1)),round(.5*size(img,2)))=1;
% SE = strel('disk',50,6);
% img=kron(img,SE.Neighborhood);

R = bwdist(img);
img = R <= 50;

[X,Y]=meshgrid(1:size(img,1));
phi=2*pi*(X+Y)./max(X(:)+Y(:))-pi;
img=img.*exp(1i*phi);

figure(1), set(gcf,'units','normalized','outerposition',[0 0 1 1]), 
clf
subplot(341),imagesc(abs(img)), axis image, title('image - intensity')
set(gca,'xtick',[],'ytick',[])
subplot(342),imagesc(angle(img)), axis image, title('image - phase')
set(gca,'xtick',[],'ytick',[])
%
imgShifted=fftshift(img);
subplot(343),imagesc(abs(imgShifted)), axis image, title('image - fftshift')
set(gca,'xtick',[],'ytick',[])
 
imgIshifted=ifftshift(img);
subplot(344),imagesc(abs(imgIshifted)), axis image, title('image - ifftshift')
set(gca,'xtick',[],'ytick',[])

fftImg=(fft2(img));
subplot(345),imagesc((abs(fftImg))), axis image,
ylabel('fft magnitude'),set(gca,'xtick',[],'ytick',[])
subplot(3,4,9),imagesc(angle(fftImg)), axis image, 
ylabel('fft phase'), xlabel('fft2(img)','Interpreter','latex')
set(gca,'xtick',[],'ytick',[])

fftImg2=fftshift(fft2(img));
subplot(346),imagesc((abs(fftImg2))), axis image,set(gca,'xtick',[],'ytick',[])
subplot(3,4,10),imagesc(angle(fftImg2)), axis image, set(gca,'xtick',[],'ytick',[])
ylabel('fft phase'), xlabel('fftshift(fft2(img))','Interpreter','latex')

fftImgShifted=fftshift(fft2(fftshift(img)));
subplot(347),imagesc((abs(fftImgShifted))), axis image, set(gca,'xtick',[],'ytick',[])
subplot(3,4,11),imagesc(angle(fftImgShifted)), axis image, set(gca,'xtick',[],'ytick',[])
xlabel('fftshift(fft2(fftshift(img)))','Interpreter','latex')
 
fftImgIshifted=fftshift(fft2(ifftshift(img)));
subplot(348),imagesc((abs(fftImgIshifted))), axis image, set(gca,'xtick',[],'ytick',[])
subplot(3,4,12),imagesc(angle(fftImgIshifted)), axis image, set(gca,'xtick',[],'ytick',[])
xlabel('fftshift(fft2($\textbf{i}$fftshift(img)))','Interpreter','latex')








